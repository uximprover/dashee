app.controller('LoginCtrl', ['$scope', '$http', function($scope, $http) {
    $scope.credentials = {};

    this.login = function() {
        $http.post('/login', {
            data: $scope.credentials
        })
        .success(function() {
            $scope.credentials = {};
            console.log('Succ');
            console.log(arguments);
        })
        .error(function() {
            console.log('Err');
            console.log(arguments);
        });
    };

    this.logout = function() {
        
    };
}]);