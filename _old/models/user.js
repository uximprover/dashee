var mongoose = require('mongoose');
var userSchema = mongoose.Schema({
    id: Schema.Types.ObjectId,
    firstname: String,
    surname: String,
    email: String,
    dashboards: [
        Schema.Types.ObjectId
    ],
    created: {
        type: Date,
        default: Date.now
    }
});
exports.schema = mongoose.model('user', userSchema);
exports.name = 'user';