var mongoose = require('mongoose');
var util = require('util');

exports.install = function(framework) {
	framework.route('/login/', view_login);
    framework.route('/login/', login, ['post']);
};

exports.models = {
    users: mongoose.user
};

function view_login() {
    var self = this;/*
    User.find(function(err, users) {
        self.view('index', users);
    });*/

    this.plain(util.inspect(this, {showHidden: false, depth: null}));

	self.view('login');
}

function login(data) {
    var self = this;
    self.plain(data);
}