var framework = require('total.js');
var http = require('http');

var port = parseInt(process.env.OPENSHIFT_NODEJS_PORT || '8080');
var ip = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var debug = true;

framework.run(http, debug, port, ip);