var mongoose = require('mongoose');
var port = parseInt(process.env.OPENSHIFT_NODEJS_PORT || '8080');
var ip = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var db_name = 'nodejs';

var mongodb_connection_string = 'mongodb://' + ip + ':' + port + '/' + db_name;
if(process.env.OPENSHIFT_MONGODB_DB_URL){
  mongodb_connection_string = process.env.OPENSHIFT_MONGODB_DB_URL + db_name;
}

mongoose.connect(mongodb_connection_string);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  console.log('Connected!');
});