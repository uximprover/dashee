exports.install = function() {
    F.route('/*', view_app);
};

framework.on('controller', function(controller, name) {  
    var language = controller.req.language;

    if (controller.query.language) {
        controller.language = controller.query.language;
        return;
    }

    controller.language = 'cz';

    if (language.indexOf('en') > -1)
        controller.language = 'en';

});

function view_app() {
    var self = this;
    self.view('login');
}