exports.install = function() {
    F.route('/login', login, ['json']);
    F.route('/login', login_view);
};

function login_view() {
    this.view('login');
}

function login() {
    console.log(this.body.data);
    this.plain('ok.');
}